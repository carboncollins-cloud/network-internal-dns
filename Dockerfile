ARG TELEGRAF_TAG=1.22.1
FROM telegraf:${TELEGRAF_TAG}

RUN apt-get update && apt-get install -y --no-install-recommends netcat && \
    rm -rf /var/lib/apt/lists/*

USER telegraf

COPY ./scripts/piHoleCollector.sh /usr/bin/piHoleCollector.sh
